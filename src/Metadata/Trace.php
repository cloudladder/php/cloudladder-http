<?php

namespace Cloudladder\Http\Metadata;

// 链路追踪trace id
final class Trace implements MetadataInterface
{
    const KEY = "x-gp-trace-id";
    private static $traceId = null;

    public static function getKey(): string
    {
        return self::KEY;
    }

    public static function getValue(): string
    {
        if (self::$traceId) {
            return self::$traceId;
        }
        self::$traceId = self::getTraceId();

        return self::$traceId;
    }

    private static function getTraceId(): string
    {
        // 上游已传直接透传
        if (function_exists("request")) {
            $hv = request()->header(self::KEY);
            if (!is_null($hv)){
                return $hv;
            }
        }
        // laravel自带的uuid方法
        if (class_exists(\Illuminate\Support\Str::class, false) && method_exists(\Illuminate\Support\Str::class, 'uuid')) {
            return \Illuminate\Support\Str::uuid()->toString();
        }
        // 保底自己生成
        return \Cloudladder\Http\Support\Str::uuid();
    }

}
