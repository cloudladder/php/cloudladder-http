<?php

namespace Cloudladder\Http\Metadata;

// 链路追踪span id
final class Span implements MetadataInterface
{
    const KEY = "x-gp-span-id";

    public static function getKey(): string
    {
        return self::KEY;
    }

    public static function getValue(): string
    {
        return self::getSpanId();
    }

    private static function getSpanId(): string
    {
        // laravel自带的uuid方法
        if (class_exists(\Illuminate\Support\Str::class, false) && method_exists(\Illuminate\Support\Str::class, 'uuid')) {
            return \Illuminate\Support\Str::uuid()->toString();
        }
        // 保底自己生成
        return \Cloudladder\Http\Support\Str::uuid();
    }

}
