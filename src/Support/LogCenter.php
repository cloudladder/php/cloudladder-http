<?php

namespace Cloudladder\Http\Support;

use GuzzleHttp\TransferStats;
use Cloudladder\Http\Metadata\Span;
use Cloudladder\Http\Metadata\Trace;
use GuzzleHttp\Client as GuzzleHttpClient;

class LogCenter
{
    public static function record(string $url, string $trace_id, string $request_input, string $response)
    {
        if(!function_exists("env")){
            return false;
        }
        if(!$log_url = env("LOG_URL")) {
            return false;
        }
        $arr = [
            'system_code' => RequestData::getAppName(),
            'route_url' => $url,
            'host_url' => '',
            'x_gp_trace_id' => $trace_id,
            'type' => 'guzzlehttp',  //日志类型
            'request_input' => $request_input,
            'response' => $response,
            'jwt' => RequestData::getToken()
        ];

        $options = [
            'json' => $arr,
            'headers' => ['Content-Type' => 'application/json']
        ];
        $client = new GuzzleHttpClient([
            'timeout' => 2, // 设置超时时间，防止记录接口异常影响业务
        ]);
        $client->post($log_url, $options); //记录日志

        return true;
    }

    /**
     * 捕获guzzlehttp请求数据
     *
     * @param  float  $beginAt
     * @return \Closure[]
     * @author Wumeng wumeng@gupo.onaliyun.com
     * @since 2023-11-10 13:54
     */
    public static function writeStats(float $beginAt)
    {
        return [
            'on_stats' => function (TransferStats $stats) use ($beginAt) {
                if (!class_exists("\Clockwork\Clockwork") ||
                    !class_exists("Clockwork\DataSource\CloudLadderDataSource") ||
                    !class_exists('\Illuminate\Foundation\Application') ||
                    !($source = app("clockwork.cloud_ladder"))
                ) {
                    return;
                }
                $request = $stats->getRequest();
                $long = $stats->getHandlerStat('url');
                $spanId = $request->getHeader(Span::getKey())[0];
                parse_str($request->getUri()->getQuery(), $query);
                $data = [
                    'trace_id' => Trace::getValue(),
                    'span_id'  => $spanId,
                    'stats'    => $stats->getHandlerStats(),
                    'query'    => $query,
                    'body'     => json_decode($request->getBody(), true) ?? new \stdClass(),
                    'app_name' => RequestData::getAppName(),
                    'method'   => $request->getMethod(),
                    'token'    => RequestData::getToken(),
                ];
                if ($stats->hasResponse()) {
                    $responseBody = json_decode($stats->getResponse()->getBody(), true);
                    $data['response_code'] = $responseBody['code'] ?? null;
                    $data['response_message'] = $responseBody['message'] ?? null;
                } else {
                    if ($stats->getHandlerErrorData() instanceof \Exception) {
                        $error = $stats->getHandlerErrorData()->getMessage();
                    } else {
                        $error = $stats->getHandlerErrorData();
                    }
                    $data['error'] = $error;
                }
                $source->writeData($data, $spanId);
                $source->performanceWrite($long, "start", $beginAt);
                $source->performanceWrite($long, "end");
            },
        ];
    }

}
