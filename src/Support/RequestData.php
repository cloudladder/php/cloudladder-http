<?php

namespace Cloudladder\Http\Support;

/**
 * Class ErrorInfo
 *
 * @package Gupo\MiddleOffice
 */
class RequestData
{
    public static function getToken()
    {
        //只考虑laravel框架中使用
        if (function_exists("request")) {
            $jwtHeader = request()->header("Authorization");
            if (!empty($jwtHeader) && preg_match("/^Bearer\\s+(.*?)$/", $jwtHeader, $matches)) {
                $jwt = $matches[1];
            }
        }
        return $jwt ?? "";
    }

    public static function getAppName()
    {
        if (function_exists("config")) {
            $app_name = config("app.name");
        }
        return $app_name ?? "";
    }

}
