<?php

namespace Cloudladder\Http;

use Cloudladder\Http\Metadata\MetadataManager;
use Cloudladder\Http\Metadata\Trace;
use Cloudladder\Http\Support\LogCenter;
use GuzzleHttp\Client as GuzzleHttpClient;
use GuzzleHttp\RequestOptions;
use Psr\Http\Message\ResponseInterface;

class Client extends GuzzleHttpClient
{
    const VERSION = "1.2.4";

    public function __construct(array $config = [])
    {
        // user-agent
        $config[RequestOptions::HEADERS] = $this->setUserAgent($config[RequestOptions::HEADERS] ?? []);
        parent::__construct($config);
    }

    // 透传metadata
    protected function passMetaData(array $header): array
    {
        $allMetadata = MetadataManager::getAllMetadata();
        foreach($allMetadata as $key => $value) {
            if (!empty($value)) {
                $header[$key] = $value;
            }
        }

        return $header;
    }

    private function setUserAgent(array $header): array
    {
        $header['User-Agent'] = sprintf("CloudladderHttp/%s", self::VERSION);

        return $header;
    }

    public function request($method, $uri = '', array $options = []): ResponseInterface
    {
        // metadata
        $options[RequestOptions::HEADERS] = $this->passMetaData($options[RequestOptions::HEADERS] ?? []);

        $response = parent::request($method, $uri, array_merge($options, LogCenter::writeStats(microtime(true))));

        // 重置响应主体流
        $response->getBody()->rewind();

        return $response;
    }
}
